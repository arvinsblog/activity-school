    /**
     * 冒泡排序函数
     * more：
     * 1. 从上一次交换索引位置开始；部分有序
     * 2. 后续已经有序，退出比较
     * @param a 待排序的数组
     * @param n 待排序的数组长度
     */
    public static void bubbleSort(int [] a, int n){
        int last = 0; // 最后一次交换的索引
        int outIndex = n - 1;
        int innerIndex = n - 1;
        for (int i = 0; i < outIndex; i++) {
            // 是否发生交换， 发生 仍未有序； 未发生 已有序
            boolean flag = false;
            if (last != 0) {
                innerIndex = last;
            }
            for (int j = 0; j < innerIndex; j++) {
                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    flag = true;
                    last = j;
                }
            }
            if (!flag) {
                break;
            }
        }
    }
