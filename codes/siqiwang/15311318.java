	/**
	 * 冒泡排序函数
	 * aa bb cc
	 * @param a 待排序的数组
	 * @param n 待排序的数组长度
	 */
	public static void bubbleSort(int [] arr, int n){
		// 你的代码，使无序数组 a 变得有序
		for (int i = 0; i < n; i++) {
			for (int j = (i + 1); j < n; j++) {
				if (arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	} //end